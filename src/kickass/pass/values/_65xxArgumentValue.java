//
// 65XX Argument Value
// for KickAssembler (with added support for 65CE02, HUC6280 & 45GS02)
// Original by Mads Nielsen
// Modified by Jesper Gravgaard
// Source code recreated from a .class file by IntelliJ IDEA (powered by FernFlower decompiler)

package kickass.pass.values;

import kickass._65xx._65xxArgType;
import kickass.common.exceptions.AsmErrorException;
import kickass.parsing.sourcelocation.SourceRange;
import kickass.pass.function.Function;
import kickass.pass.function.table.FunctionTable;
import kickass.pass.function.table.StdFunctionTable;
import kickass.pass.valuerep.ValueRepresentation;
import kickass.state.EvaluationState;

// Modification: Added value3

public class _65xxArgumentValue extends ImmutableValue {
    private static ValueRepresentation _65xxArgRepresentation = new ValueRepresentation("65xxArg");
    public static _65xxArgumentValue noArgument;
    private _65xxArgType type;
    private Value value;
    private Value value2;
    private Value value3;
    static FunctionTable functions;

    public static boolean has65xxArg(Value var0) {
        return var0.hasRepresentation(_65xxArgRepresentation);
    }

    public static _65xxArgumentValue get65xxArg(Value var0, SourceRange var1) {
        _65xxArgumentValue var2 = (_65xxArgumentValue)var0.getRepresentation(_65xxArgRepresentation);
        if (var2 == null) {
            throw new AsmErrorException("Can't get a 65xxArgument from value of type " + var0.getType(), var1);
        } else {
            return var2;
        }
    }

    public _65xxArgumentValue(_65xxArgType type, Value value, Value value2, Value value3) {
        this.type = type;
        this.value = value;
        this.value2 = value2;
        this.value3 = value3;
    }

    public _65xxArgumentValue(_65xxArgType type, Value value, Value value2) {
        this(type, value, value2, NumberValue.zero);
    }

    public Object getRepresentation(ValueRepresentation var1) {
        return var1 == _65xxArgRepresentation ? this : null;
    }

    public boolean hasString() {
        return true;
    }

    public String getString(SourceRange var1) {
        return this.isInvalid() ? "<<Invalid AsmCmdArg>>" : "Argument<" + this.type + ">";
    }

    public String getType() {
        return "65xxArgumentValue";
    }

    public _65xxArgType get65xxArgType() {
        return this.type;
    }

    public Value getValue() {
        return this.value;
    }

    public Value getValue2() {
        return this.value2;
    }

    public Value getValue3() {
        return this.value3;
    }

    public FunctionTable getFunctions() {
        return functions;
    }

    public boolean hasInvalidContent() {
        return this.value.isInvalidOrInvalidContent() || this.value2.isInvalidOrInvalidContent() || this.value3.isInvalidOrInvalidContent();
    }

    static {
        noArgument = new _65xxArgumentValue(_65xxArgType.noArgument, NumberValue.zero, NumberValue.zero, NumberValue.zero);
        functions = new StdFunctionTable(getStandardFunctions());
        functions.add(new Function("getType", 1, NumberValue.invalid) {
            public Value execute(Value[] var1, EvaluationState var2, SourceRange var3) {
                return new NumberValue((double)((_65xxArgumentValue)var1[0]).get65xxArgType().getIdNo());
            }
        });
        functions.add(new Function("getValue", 1, NumberValue.invalid) {
            public Value execute(Value[] var1, EvaluationState var2, SourceRange var3) {
                return ((_65xxArgumentValue)var1[0]).value;
            }
        });
    }
}
