package kickass._65xx.cpus;

import java.util.ArrayList;
import java.util.List;

// HUC6280 CPU, Mnemonics and Opcodes
// for KickAssembler (with added support for 65CE02, 45GS02 and HUC6280)

// Original by Mads Nielsen
// Modified by Jesper Gravgaard

public class CPU_HUC6280 {
   public static String name = "_huc6280";
   public static Cpu instance;
   public static List<MnemonicDefinition> mnemonics_diffTo65c02 = new ArrayList();

   public CPU_HUC6280() {
   }

   static {
      // int[] is opcode for each addressing mode: NONE, IMMEDIATE, ZEROPAGE, ZEROPAGEX, ZEROPAGEY, IZEROPAGEX, IZEROPAGEY, ABSOLUTE, ABSOLUTEX, ABSOLUTEY, INDIRECT, RELATIVE, IZEROPAGE, ZPRELATIVE, INDIRECTX, ZEROPAGEZ, ISTACKZEROPAGEY, IMMEDIATEW, RELATIVEW, IMMEDIATEANDZP, IMMEDIATEANDABSOLUTE, IMMEDIATEANDZPX, IMMEDIATEANDABSOLUTEX, ABS_ABS_ABS
      mnemonics_diffTo65c02.add(new MnemonicDefinition("sxy", new int[]{     2,        -1,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("st0", new int[]{    -1,         3,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("st1", new int[]{    -1,        19,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("sax", new int[]{    34,        -1,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("st2", new int[]{    -1,        35,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("say", new int[]{    66,        -1,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("tma", new int[]{    -1,        67,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("bsr", new int[]{    -1,        -1,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       68,        -1,         -1,        -1,        -1,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("tam", new int[]{    -1,        83,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("csl", new int[]{     84,        -1,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("cla", new int[]{     98,        -1,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("clx", new int[]{    130,        -1,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("cly", new int[]{    194,        -1,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("csh", new int[]{    212,        -1,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("set", new int[]{    244,        -1,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("tst", new int[]{     -1,        -1,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        -1,     -1, -1, 131, 147, 163, 179 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("tia", new int[]{     -1,        -1,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        -1,     -1, -1, -1, -1, -1, -1, 227 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("tdd", new int[]{     -1,        -1,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        -1,     -1, -1, -1, -1, -1, -1, 195 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("tin", new int[]{     -1,        -1,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        -1,     -1, -1, -1, -1, -1, -1, 211 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("tii", new int[]{     -1,        -1,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        -1,     -1, -1, -1, -1, -1, -1, 115 }));
      instance = new Cpu(name);
      instance.setMnemonics(CPU_65C02.instance.mnemonics, mnemonics_diffTo65c02);
   }

}
