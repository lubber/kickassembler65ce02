package kickass._65xx.cpus;

import java.util.*;

// 45GS02 CPU, Mnemonics and Opcodes
// for KickAssembler (with added support for 65CE02 & 45GS02)
// Original by Mads Nielsen
// Modified by Jesper Gravgaard

public class CPU_45GS02 {
   public static String name = "_45gs02";
   public static Cpu instance;
   public static List<MnemonicDefinition> mnemonics_diffTo65ce02 = new ArrayList();

   public CPU_45GS02() {
   }

   /** The byte ( NOP / 234 ) to prefix to the opcode when generating a 32-bit addressing instruction. Used for addressing mode 32IZEROPAGEZ and 32IZEROPAGE. */
   public static byte A32_OPCODE_PREFIX = (byte) 234;
   /** The byte ( NEG / 66 ) to prefix twice to the opcode when generating an instruction working on the virtual 32-bit register Q. Used for all Q-instructions. */
   public static byte R32_OPCODE_PREFIX = (byte) 66;

   /**
    * Determines if a mnemonic works on the virtual 32-bit register Q.
    * If it does the output code will have 2 bytes ( NEG NEG / 66 66 ) prefixed before the instruction opcode.
    * @param mnemonic The mnemonic to examine
    * @return true if the mnemonic works on the virtual 32-bit register Q.
    */
   public static boolean isR32Mnemonic(String mnemonic) {
      return R32_MNEMONICS.contains(mnemonic);
   }

   // All mnemonic that works the virtual 32-bit register Q.
   public static List<String> R32_MNEMONICS = Arrays.asList("adcq", "andq", "aslq", "asrq", "bitq", "cpq", "deq", "eorq", "inq", "ldq", "lsrq", "orq", "rolq", "rorq", "sbcq", "stq");

   static {
      // int[] is opcode for each addressing mode:  NONE, IMMEDIATE, ZEROPAGE, ZEROPAGEX, ZEROPAGEY, IZEROPAGEX, IZEROPAGEY, ABSOLUTE, ABSOLUTEX, ABSOLUTEY, INDIRECT, RELATIVE, IZEROPAGE, ZPRELATIVE, INDIRECTX, IZEROPAGEZ, ISTACKZEROPAGEY, IMMEDIATEW, RELATIVEW, 32IZEROPAGEZ, 32IZEROPAGE
      mnemonics_diffTo65ce02.add(new MnemonicDefinition("adc", new int[]{    -1,       105,      101,       117,        -1,         97,        113,      109,        125,      121,       -1,       -1,        -1,         -1,        -1,       114,               -1,         -1,        -1,          114,          -1  }));
      mnemonics_diffTo65ce02.add(new MnemonicDefinition("and", new int[]{    -1,        41,       37,        53,        -1,         33,         49,       45,         61,       57,       -1,       -1,        -1,         -1,        -1,        50,               -1,         -1,        -1,           50,          -1  }));
      mnemonics_diffTo65ce02.add(new MnemonicDefinition("cmp", new int[]{    -1,       201,      197,       213,        -1,        193,        209,      205,        221,      217,       -1,       -1,        -1,         -1,        -1,       210,               -1,         -1,        -1,          210,          -1  }));
      mnemonics_diffTo65ce02.add(new MnemonicDefinition("eor", new int[]{    -1,        73,       69,        85,        -1,         65,         81,       77,         93,       89,       -1,       -1,        -1,         -1,        -1,        82,               -1,         -1,        -1,           82,          -1  }));
      mnemonics_diffTo65ce02.add(new MnemonicDefinition("ora", new int[]{    -1,         9,        5,        21,        -1,          1,         17,       13,         29,       25,       -1,       -1,        -1,         -1,        -1,        18,               -1,         -1,        -1,           18,          -1  }));
      mnemonics_diffTo65ce02.add(new MnemonicDefinition("sbc", new int[]{    -1,       233,      229,       245,        -1,        225,        241,      237,        253,      249,       -1,       -1,        -1,         -1,        -1,       242,               -1,         -1,        -1,          242,          -1  }));
      mnemonics_diffTo65ce02.add(new MnemonicDefinition("lda", new int[]{    -1,       169,      165,       181,        -1,        161,        177,      173,        189,      185,       -1,       -1,        -1,         -1,        -1,       178,              226,         -1,        -1,          178,          -1  }));
      mnemonics_diffTo65ce02.add(new MnemonicDefinition("sta", new int[]{    -1,        -1,      133,       149,        -1,        129,        145,      141,        157,      153,       -1,       -1,        -1,         -1,        -1,       146,              130,         -1,        -1,          146,          -1  }));
      mnemonics_diffTo65ce02.add(new MnemonicDefinition("adcq", new int[]{   -1,        -1,      101,        -1,        -1,         -1,         -1,      109,         -1,       -1,       -1,       -1,       114,         -1,        -1,        -1,               -1,         -1,        -1,           -1,         114  }));
      mnemonics_diffTo65ce02.add(new MnemonicDefinition("andq", new int[]{   -1,        -1,       37,        -1,        -1,         -1,         -1,       45,         -1,       -1,       -1,       -1,        50,         -1,        -1,        -1,               -1,         -1,        -1,           -1,          50  }));
      mnemonics_diffTo65ce02.add(new MnemonicDefinition("aslq", new int[]{   10,        -1,        6,        22,        -1,         -1,         -1,       14,         30,       -1,       -1,       -1         -1,         -1,        -1,        -1,               -1,         -1,        -1,           -1,          -1  }));
      mnemonics_diffTo65ce02.add(new MnemonicDefinition("asrq", new int[]{   67,        -1,       68,        84,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1         -1,         -1,        -1,        -1,               -1,         -1,        -1,           -1,          -1  }));
      mnemonics_diffTo65ce02.add(new MnemonicDefinition("bitq", new int[]{   -1,        -1,       36,        -1,        -1,         -1,         -1,       44,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,               -1,         -1,        -1,           -1,          -1  }));
      mnemonics_diffTo65ce02.add(new MnemonicDefinition("cpq",  new int[]{   -1,        -1,      197,        -1,        -1,         -1,         -1,      205,         -1,       -1,       -1,       -1,       210,         -1,        -1,        -1,               -1,         -1,        -1,           -1,         210  }));
      mnemonics_diffTo65ce02.add(new MnemonicDefinition("deq",  new int[]{   58,        -1,      198,       214,        -1,         -1,         -1,      206,        222,       -1,       -1,       -1,        -1,         -1,        -1,        -1,               -1,         -1,        -1,           -1,          -1  }));
      mnemonics_diffTo65ce02.add(new MnemonicDefinition("eorq", new int[]{   -1,        -1,       69,        -1,        -1,         -1,         -1,       77,         -1,       -1,       -1,       -1,        82,         -1,        -1,        -1,               -1,         -1,        -1,           -1,          82  }));
      mnemonics_diffTo65ce02.add(new MnemonicDefinition("inq",  new int[]{   26,        -1,      230,       246,        -1,         -1,         -1,      238,        254,       -1,       -1,       -1,        -1,         -1,        -1,        -1,               -1,         -1,        -1,           -1,          -1  }));
      mnemonics_diffTo65ce02.add(new MnemonicDefinition("ldq",  new int[]{   -1,        -1,      165,       181,        -1,        161,        177,      173,        189,      185,       -1,       -1,        -1,         -1,        -1,       178,              226,         -1,        -1,          178,          -1  }));
      mnemonics_diffTo65ce02.add(new MnemonicDefinition("lsrq", new int[]{   74,        -1,       70,        86,        -1,         -1,         -1,       78,         94,       -1,       -1,       -1         -1,         -1,        -1,        -1,               -1,         -1,        -1,           -1,          -1  }));
      mnemonics_diffTo65ce02.add(new MnemonicDefinition("orq",  new int[]{   -1,        -1,        5,        -1,        -1,          1,         -1,       13,         -1,       -1,       -1,       -1,        18,         -1,        -1,        -1,               -1,         -1,        -1,           -1,          18  }));
      mnemonics_diffTo65ce02.add(new MnemonicDefinition("rolq", new int[]{   42,        -1,       38,        54,        -1,         -1,         -1,       46,         62,       -1,       -1,       -1         -1,         -1,        -1,        -1,               -1,         -1,        -1,           -1,          -1  }));
      mnemonics_diffTo65ce02.add(new MnemonicDefinition("rorq", new int[]{  106,        -1,      102,       118,        -1,         -1,         -1,      110,        126,       -1,       -1,       -1         -1,         -1,        -1,        -1,               -1,         -1,        -1,           -1,          -1  }));
      mnemonics_diffTo65ce02.add(new MnemonicDefinition("sbcq", new int[]{   -1,        -1,      229,        -1,        -1,         -1,         -1,      237,         -1,       -1,       -1,       -1,       242,         -1,        -1,        -1,               -1,         -1,        -1,           -1,         242  }));
      mnemonics_diffTo65ce02.add(new MnemonicDefinition("stq",  new int[]{   -1,        -1,      133,        -1,        -1,         -1,         -1,      141,         -1,       -1,       -1,       -1,       146,         -1,        -1,        -1,              130,         -1,        -1,           -1,         146  }));
      instance = new Cpu(name);
      instance.setMnemonics(CPU_65CE02.instance.mnemonics, mnemonics_diffTo65ce02);
   }

}
