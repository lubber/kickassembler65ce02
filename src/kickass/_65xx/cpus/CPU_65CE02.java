package kickass._65xx.cpus;

import java.util.ArrayList;
import java.util.List;

// 65CE02 CPU, Mnemonics and Opcodes
// for KickAssembler (with added support for 65CE02 & 45GS02)

// Original by Mads Nielsen
// Modified by Jesper Gravgaard

public class CPU_65CE02 {
   public static String name = "_65ce02";
   public static Cpu instance;
   public static List<MnemonicDefinition> mnemonics_diffTo65c02 = new ArrayList();

   public CPU_65CE02() {
   }

   static {
      // int[] is opcode for each addressing mode: NONE, IMMEDIATE, ZEROPAGE, ZEROPAGEX, ZEROPAGEY, IZEROPAGEX, IZEROPAGEY, ABSOLUTE, ABSOLUTEX, ABSOLUTEY, INDIRECT, RELATIVE, IZEROPAGE, ZPRELATIVE, INDIRECTX, ZEROPAGEZ, ISTACKZEROPAGEY, IMMEDIATEW, RELATIVEW
      mnemonics_diffTo65c02.add(new MnemonicDefinition("ldz", new int[]{    -1,       163,       -1,        -1,        -1,         -1,         -1,      171,       187,        -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("sty", new int[]{    -1,        -1,      132,       148,        -1,         -1,         -1,      140,        139,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("stx", new int[]{    -1,        -1,      134,        -1,       150,         -1,         -1,      142,         -1,      155,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("jsr", new int[]{    -1,        -1,       -1,        -1,        -1,         -1,         -1,       32,         -1,       -1,       34,       -1,        -1,         -1,        35,        -1,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("cpz", new int[]{    -1,       194,      212,        -1,        -1,         -1,         -1,      220,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("rtn", new int[]{    -1,        98,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("asr", new int[]{    67,        -1,       68,        84,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("asw", new int[]{    -1,        -1,       -1,        -1,        -1,         -1,         -1,      203,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("dew", new int[]{    -1,        -1,      195,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("inw", new int[]{    -1,        -1,      227,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("row", new int[]{    -1,        -1,       -1,        -1,        -1,         -1,         -1,      235,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("cle", new int[]{     2,        -1,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("dez", new int[]{    59,        -1,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("inz", new int[]{    27,        -1,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("map", new int[]{    92,        -1,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("neg", new int[]{    66,        -1,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("phz", new int[]{   219,        -1,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("plz", new int[]{   251,        -1,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("see", new int[]{     3,        -1,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("tab", new int[]{    91,        -1,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("taz", new int[]{    75,        -1,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("tba", new int[]{   123,        -1,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("tsy", new int[]{    11,        -1,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("tys", new int[]{    43,        -1,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("tza", new int[]{   107,        -1,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("adc", new int[]{    -1,       105,      101,       117,        -1,         97,        113,      109,        125,      121,       -1,       -1,        -1,         -1,        -1,       114,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("and", new int[]{    -1,        41,       37,        53,        -1,         33,         49,       45,         61,       57,       -1,       -1,        -1,         -1,        -1,        50,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("cmp", new int[]{    -1,       201,      197,       213,        -1,        193,        209,      205,        221,      217,       -1,       -1,        -1,         -1,        -1,       210,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("eor", new int[]{    -1,        73,       69,        85,        -1,         65,         81,       77,         93,       89,       -1,       -1,        -1,         -1,        -1,        82,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("ora", new int[]{    -1,         9,        5,        21,        -1,          1,         17,       13,         29,       25,       -1,       -1,        -1,         -1,        -1,        18,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("sbc", new int[]{    -1,       233,      229,       245,        -1,        225,        241,      237,        253,      249,       -1,       -1,        -1,         -1,        -1,       242,              -1,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("lda", new int[]{    -1,       169,      165,       181,        -1,        161,        177,      173,        189,      185,       -1,       -1,        -1,         -1,        -1,       178,             226,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("sta", new int[]{    -1,        -1,      133,       149,        -1,        129,        145,      141,        157,      153,       -1,       -1,        -1,         -1,        -1,       146,             130,         -1,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("phw", new int[]{    -1,        -1,       -1,        -1,        -1,         -1,         -1,      252,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,        244,        -1 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("lbcc", new int[]{   -1,        -1,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,       147 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("lbcs", new int[]{   -1,        -1,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,       179 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("lbeq", new int[]{   -1,        -1,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,       243 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("lbne", new int[]{   -1,        -1,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,       211 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("lbmi", new int[]{   -1,        -1,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        51 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("lbpl", new int[]{   -1,        -1,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        19 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("lbra", new int[]{   -1,        -1,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,       131 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("lbsr", new int[]{   -1,        -1,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        99 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("lbvc", new int[]{   -1,        -1,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        83 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("lbvs", new int[]{   -1,        -1,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,       115 }));
      mnemonics_diffTo65c02.add(new MnemonicDefinition("eom",  new int[]{  234,        -1,       -1,        -1,        -1,         -1,         -1,       -1,         -1,       -1,       -1,       -1,        -1,         -1,        -1,        -1,              -1,         -1,        -1 }));
      instance = new Cpu(name);
      instance.setMnemonics(CPU_65C02.instance.mnemonics, mnemonics_diffTo65c02);
   }

}
