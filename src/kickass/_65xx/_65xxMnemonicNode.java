//
// 65XX Mnemonic Mode
// for KickAssembler (with added support for 65CE02, HUC6280 & 45GS02)
// Original by Mads Nielsen
// Modified by Jesper Gravgaard
// Source code recreated from a .class file by IntelliJ IDEA (powered by FernFlower decompiler)

package kickass._65xx;

import kickass._65xx.cpus.CPU_45GS02;
import kickass.common.diagnostics.AsmError;
import kickass.common.diagnostics.AsmWarning;
import kickass.common.exceptions.AsmErrorException;
import kickass.parsing.sourcelocation.SourceRange;
import kickass.pass.asmnode.AsmNode;
import kickass.pass.asmnode.directives.AsmDirective;
import kickass.pass.asmnode.output.SideEffectOnlyOutput;
import kickass.pass.values.LabelValue;
import kickass.pass.values.NumberValue;
import kickass.pass.values.Value;
import kickass.pass.values._65xxArgumentValue;
import kickass.state.EvaluationState;
import kickass.state.scope.ResolvedSymbol;
import kickass.state.scope.SymbolScope;
import kickass.state.scope.symboltable.ISymbolPageIdx;
import kickass.state.scope.symboltable.SymbolStatus;
import kickass.state.segments.SegmentMemoryBlock.Entry;

public class _65xxMnemonicNode extends AsmDirective {
    private final String mnemonic;
    private final _65xxArgumentExpr argExpr;
    private final int[] modes;
    private _65xxArgType argType;
    private final String argLabelName;
    private final _65xxMnemonicNode.ForceMode forceMode;
    private ISymbolPageIdx argLabelsymbolPageIdx;
    private boolean labelIsResolved = false;
    private Entry memEntry;
    private boolean hasNotUsedZpMode;

    private static _65xxArgType resolveExtension(String extension, SourceRange sourceRange, EvaluationState state) {
        if (!extension.equals("imm") && !extension.equals("im")) {
            if (!extension.equals("zpx") && !extension.equals("zx")) {
                if (!extension.equals("zpy") && !extension.equals("zy")) {
                    if (!extension.equals("izpx") && !extension.equals("izx")) {
                        if (!extension.equals("izpy") && !extension.equals("izy")) {
                            if (!extension.equals("absx") && !extension.equals("ax")) {
                                if (!extension.equals("absy") && !extension.equals("ay")) {
                                    if (!extension.equals("ind") && !extension.equals("i")) {
                                        if (!extension.equals("rel") && !extension.equals("r")) {
                                            state.diagnosticMgr.add(new AsmError("Unknown extension: " + extension, sourceRange));
                                            return null;
                                        } else {
                                            return _65xxArgType.relative;
                                        }
                                    } else {
                                        return _65xxArgType.indirect;
                                    }
                                } else {
                                    return _65xxArgType.absoluteY;
                                }
                            } else {
                                return _65xxArgType.absoluteX;
                            }
                        } else {
                            return _65xxArgType.indirectZeropageY;
                        }
                    } else {
                        return _65xxArgType.indirectZeropageX;
                    }
                } else {
                    return _65xxArgType.zeropageY;
                }
            } else {
                return _65xxArgType.zeropageX;
            }
        } else {
            return _65xxArgType.immediate;
        }
    }

    public static _65xxMnemonicNode createNode(String mnemonic, String extension, _65xxParsedArg parsedArg, int[] modes, String var4, SourceRange sourceRange, EvaluationState state) {
        _65xxMnemonicNode.ForceMode forceMode = null;
        _65xxArgType argType = null;
        if (extension != null) {
            if (!extension.equals("zp") && !extension.equals("z")) {
                if (!extension.equals("abs") && !extension.equals("a")) {
                    argType = resolveExtension(extension, sourceRange, state);
                    if (argType == null) {
                        return null;
                    }
                } else {
                    forceMode = _65xxMnemonicNode.ForceMode.abs;
                }
            } else {
                forceMode = _65xxMnemonicNode.ForceMode.zp;
            }
        }

        boolean isUnresolvedAbsolute = parsedArg.type == _65xxArgType.unresolvedAbsolute;
        _65xxArgumentExpr argumentExpr = new _65xxArgumentExpr(parsedArg.type, parsedArg.expr1, parsedArg.expr2, parsedArg.expr3, isUnresolvedAbsolute);
        return new _65xxMnemonicNode(mnemonic, argType, argumentExpr, modes, var4, forceMode, sourceRange);
    }

    public _65xxMnemonicNode(String mnemonic, _65xxArgType argType, _65xxArgumentExpr argExpr, int[] modes, String argLabelName, _65xxMnemonicNode.ForceMode forceMode, SourceRange sourceRange) {
        super(sourceRange);
        this.argExpr = argExpr;
        this.mnemonic = mnemonic;
        this.argType = argType;
        this.modes = modes;
        this.argLabelName = argLabelName;
        this.forceMode = forceMode;
    }

    private _65xxMnemonicNode(_65xxMnemonicNode orig) {
        super(orig.range);
        this.argExpr = orig.argExpr;
        this.mnemonic = orig.mnemonic;
        this.argType = orig.argType;
        this.modes = orig.modes;
        this.argLabelName = orig.argLabelName;
        this.argLabelsymbolPageIdx = orig.argLabelsymbolPageIdx;
        this.forceMode = orig.forceMode;
    }

    public AsmNode copy() {
        return new _65xxMnemonicNode(this);
    }

    public AsmNode executeMetaRegistrations(EvaluationState var1) {
        if (this.argLabelName != null) {
            boolean var2 = this.argLabelName.startsWith("@");
            String var3 = var2 ? this.argLabelName.substring(1) : this.argLabelName;
            SymbolScope var4 = var1.scopeMgr.getRootOrCurrentScope(var2);
            if (var4.isDefined(var3)) {
                var1.diagnosticMgr.add(new AsmError("The symbol '" + var3 + "' is already defined", this.range));
                return this;
            }

            this.argLabelsymbolPageIdx = var4.define(var3, LabelValue.getInitializer(var1));
            this.argLabelsymbolPageIdx.setStatus(SymbolStatus.defined);
            this.argLabelsymbolPageIdx.setIsScopeExposed(true);
        }

        return this;
    }

    public AsmNode executePrepass(EvaluationState var1) {
        this.argExpr.executePrepass(var1);
        return this;
    }

    public AsmNode executePass(EvaluationState state) {
        state.sideeffectMgr.clearFunctionSideOutput();
        if (state.getPassNo() == 1) {
            this.memEntry = state.segmentMgr.getCurrentSegment().getCurrentMemoryBlock().createNewEntry();
            if (this.argLabelName != null) {
                LabelValue labelValue = (LabelValue) this.argLabelsymbolPageIdx.getValueHolder().getWithoutSideeffect();
                labelValue.resolveSegment(state.segmentMgr.getCurrentSegment().getBank());
            }
        }

        boolean memoryPositionNegative = false;
        if (this.argLabelName != null && !this.labelIsResolved) {
            int memoryPosition = state.segmentMgr.getMemoryPosition();
            if (memoryPosition < 0) {
                memoryPositionNegative = true;
            } else {
                LabelValue labelValue = (LabelValue) this.argLabelsymbolPageIdx.getValueHolder().getWithoutSideeffect();
                labelValue.resolveLabel(new NumberValue(memoryPosition + 1));
                state.scopeMgr.addResolvedSymbol(new ResolvedSymbol(this.argLabelName, memoryPosition + 1, state.segmentMgr.getCurrentSegment(), this.range));
                state.setMadeMetaProgress();
                this.labelIsResolved = true;
            }
        }

        Value argExprValue = this.argExpr.evaluate(state);
        _65xxArgumentValue argVal = _65xxArgumentValue.get65xxArg(argExprValue, this.range);
        Value argValue = argVal.getValue();
        Long argValueInt = !argValue.isInvalid() && argVal.get65xxArgType() != _65xxArgType.noArgument ? argValue.getLong(this.argExpr.valueExpr.getSourceRange()) : null;
        _65xxArgType argType;
        boolean modeSupported;
        if (this.argType == null) {
            this.argType = argVal.get65xxArgType();
            boolean forceModeZp = this.forceMode == _65xxMnemonicNode.ForceMode.zp;
            if (forceModeZp) {
                argType = this.argType.getZpForm();
                if (argType == null) {
                    throw new AsmErrorException("Cannot force zeropage mode on an argument of type " + this.argType.name(), this.range);
        }

                modeSupported = this.getMode(argType.getIdNo(), this.modes) != -1;
                if (!modeSupported) {
                    throw new AsmErrorException("'" + this.mnemonic + "' does not support mode '" + argType.name() + "'", this.range);
                }
            }
        }

        if (!this.argType.isResolved()) {
            _65xxArgType argTypeBefore = this.argType;
            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            // START: MODIFIED FOR 65CE02
            final Value argValue2 = argVal.getValue2();
            this.argType = this.resolveArgType(this.argType, argValue, argValue2, this.modes);
            // END: MODIFIED FOR 65CE02
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            if (argValueInt == null) {
                argType = argTypeBefore.getZpForm();
                modeSupported = argType != null && this.getMode(argType.getIdNo(), this.modes) >= 0;
                this.hasNotUsedZpMode = modeSupported && argType != this.argType;
            }
        }

        int totalByteSize = 1 + this.argType.getByteSize();
        // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        // START: ADDED FOR 45GS02 SUPPORT
        // Add the extra size of prefix opcodes
        if (this.argType == _65xxArgType.indirect32ZeropageZ || this.argType == _65xxArgType.indirect32Zeropage) {
            // Add one byte for the prefixed NOP (opcode 234) used for 32-bit addressing
            totalByteSize += 1;
        }
        if (CPU_45GS02.isR32Mnemonic(this.mnemonic)) {
            // Add 2 bytes for the prefixed NEG NEG (opcode 66) used for the 32-bit virtual register (Q)
            totalByteSize += 2;
        }
        // END: ADDED FOR 45GS02 SUPPORT
        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        state.segmentMgr.increaseMemoryPosition(totalByteSize);
        this.memEntry.setNoOfBytes(totalByteSize);
        int opcode = this.modes.length <= this.argType.getIdNo() ? -1 : this.modes[this.argType.getIdNo()];
        if (opcode == -1) {
            throw new AsmErrorException("invalid adressingmode. '" + this.mnemonic + "' doesn't support " + this.argType + " mode", this.range);
        } else if (argValue.isInvalid()) {
            return this;
        } else {
            if (this.argType == _65xxArgType.relative) {
                int memoryPosition = state.segmentMgr.getMemoryPosition();
                if (memoryPosition < 0) {
                    return this;
                }

                argValueInt = argValueInt - memoryPosition;
                if (argValueInt < -128 || 127 < argValueInt) {
                    throw new AsmErrorException("relative address is illegal (jump distance is too far).", this.argExpr.valueExpr.getSourceRange());
                }
            }
            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            // START: ADDED FOR 65CE02 SUPPORT
            // Add support for Relative Word
            else if (this.argType == _65xxArgType.relativeWord) {
                int memoryPosition = state.segmentMgr.getMemoryPosition();
                if (memoryPosition < 0) {
                    return this;
                }
                argValueInt = argValueInt - memoryPosition + 1;
            }
            // END: ADDED FOR 65CE02 SUPPORT
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            int memoryPosition;
            if (this.argType == _65xxArgType.zeropageRelative) {
                Value value2 = argVal.getValue2();
                if (value2.isInvalid()) {
                    return this;
                }

                Long value2Int = value2.getLong(this.range);
                memoryPosition = state.segmentMgr.getMemoryPosition();
                if (memoryPosition < 0) {
                    return this;
                }

                value2Int = value2Int - memoryPosition;
                if (value2Int < -128 || 127 < value2Int) {
                    throw new AsmErrorException("relative address is illegal (jump distance is too far).", this.argExpr.valueExpr2.getSourceRange());
                }

                argValueInt = argValueInt & 255 | (value2Int & 255) << 8;
            }

            // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            // START: ADDED FOR HUC6280 SUPPORT
            if (this.argType == _65xxArgType.immediateAndZeropage || this.argType == _65xxArgType.immediateAndAbsolute || this.argType == _65xxArgType.immediateAndZeropageX || this.argType == _65xxArgType.immediateAndAbsoluteX) {
                Value value2 = argVal.getValue2();
                if (value2.isInvalid()) {
                    return this;
                }
                Long value2Int = value2.getLong(this.range);
                if(this.argType.equals(this.argType.getZpForm()))
                    value2Int = value2Int & 255;
                argValueInt = argValueInt & 255 | (value2Int) << 8;
            }
            if (this.argType == _65xxArgType.absoluteAbsoluteAbsolute) {
                Value value2 = argVal.getValue2();
                if (value2.isInvalid()) {
                    return this;
                }
                Long value2Int = value2.getLong(this.range);
                Value value3 = argVal.getValue3();
                if (value3.isInvalid()) {
                    return this;
                }
                Long value3Int = value3.getLong(this.range);
                argValueInt = (argValueInt & 0xffff) | (value2Int&0xffff) << 16| (value3Int&0xffff) << 32;
            }
            // END: ADDED FOR HUC6280 SUPPORT
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            if (!memoryPositionNegative && !state.sideeffectMgr.getErrorOrInvalidSideEffectsDuringFunctionEvaluation()) {
                boolean zpModeProblem = this.hasNotUsedZpMode && 0 <= argValueInt && argValueInt < 256;
                if (zpModeProblem) {
                    state.diagnosticMgr.add(new AsmWarning("Using absolute mode for zeropage argument. Try defining involved labels before this statement.", this.range));
                }

                byte[] bytes = new byte[totalByteSize];
                // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                // START: EDITED FOR 45GS02 SUPPORT
                // Add support for 32-bit addressing (by prefixing a NOP) and teh 32-bit virtual register (by prefixing NEG NEG)
                int idx = 0;
                if (CPU_45GS02.isR32Mnemonic(this.mnemonic)) {
                    // Add a prefix NEG NEG (opcode 66) to signal using the 32-bit virtual register
                    bytes[idx++] = CPU_45GS02.R32_OPCODE_PREFIX;
                    bytes[idx++] = CPU_45GS02.R32_OPCODE_PREFIX;
                }
                if (this.argType == _65xxArgType.indirect32ZeropageZ || this.argType == _65xxArgType.indirect32Zeropage) {
                    // Add a prefix NOP (opcode 234) to signal 32-bit addressing
                    bytes[idx++] = CPU_45GS02.A32_OPCODE_PREFIX;
                }
                bytes[idx++] = (byte) opcode;
                while (idx < totalByteSize) {
                    bytes[idx++] = (byte) (argValueInt & 255);
                    argValueInt = argValueInt >> 8;
                }
                // END: EDITED FOR 45GS02 SUPPORT
                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                this.memEntry.fillEntry(bytes, this.range);
                state.setMadeMetaProgress();
                return SideEffectOnlyOutput.create(state.sideeffectMgr.getFunctionSideOutput());
            } else {
                return this;
            }
        }
    }

    // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    // START: MODIFIED FOR 65CE02 SUPPORT
    private _65xxArgType resolveArgType(_65xxArgType argType, Value argValue, Value argValue2, int[] mnemonicModes) {
        Integer argValueInt = !argValue.isInvalid() ? argValue.getInt(this.argExpr.valueExpr.getSourceRange()) : null;
        Integer argValue2Int = !argValue.isInvalid() ? argValue2.getInt(this.argExpr.valueExpr.getSourceRange()) : null;
        final boolean zeropageBound = argValue.isZeropageBound();
        if (this.forceMode == _65xxMnemonicNode.ForceMode.abs) {
            return argType.getAbsForm();
        } else if (argType.equals(_65xxArgType.unresolvedImmediate)) {
            final boolean hasImmWordMode = this.getMode(_65xxArgType.immediateWord.getIdNo(), mnemonicModes) >= 0;
            if (hasImmWordMode) {
                return _65xxArgType.immediateWord;
            } else {
                return _65xxArgType.immediate;
            }
        } else if(argType.equals(_65xxArgType.unresolvedImmediateAndAbsolute) || argType.equals(_65xxArgType.unresolvedImmediateAndAbsoluteX)) {
            boolean isZpValue = argValue2Int != null && argValue2Int <= 255;
            final boolean zeropageBound2 = argValue2.isZeropageBound();
            if ((zeropageBound2 || isZpValue || this.forceMode == _65xxMnemonicNode.ForceMode.zp))
                return argType.getZpForm();
            else
                return argType.getAbsForm();
        } else {
            _65xxArgType argZpForm = argType.getZpForm();
            boolean hasZpForm = argZpForm != null && this.getMode(argZpForm.getIdNo(), mnemonicModes) >= 0;
            boolean isZpValue = argValueInt != null && argValueInt <= 255;
            if (hasZpForm && (zeropageBound || isZpValue || this.forceMode == _65xxMnemonicNode.ForceMode.zp)) {
                return argZpForm;
            } else {
                final boolean thisHasRelativeMode = this.getMode(_65xxArgType.relative.getIdNo(), mnemonicModes) >= 0;
                final boolean thisHasRelativeWordMode = this.getMode(_65xxArgType.relativeWord.getIdNo(), mnemonicModes) >= 0;
                final boolean argHasRelativeMode = argType.getRelForm() != null;
                if (argHasRelativeMode && thisHasRelativeMode) {
                    return _65xxArgType.relative;
                } else if (argHasRelativeMode && thisHasRelativeWordMode) {
                    return _65xxArgType.relativeWord;
                } else {
                    return argType.getAbsForm();
                }
            }
        }
    }
    // END: MODIFIED FOR 65CE02 SUPPORT
    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    private int getMode(int idNo, int[] mnemonicModes) {
        return idNo >= mnemonicModes.length ? -1 : mnemonicModes[idNo];
    }

    public String toString() {
        return this.mnemonic;
    }

    private static enum ForceMode {
        abs,
        zp;

        private ForceMode() {
        }
    }
}
