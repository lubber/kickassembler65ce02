//
// 65XX Argument Type
// for KickAssembler (with added support for 65CE02, HUC6280 & 45GS02)
// Original by Mads Nielsen
// Modified by Jesper Gravgaard
// Source code recreated from a .class file by IntelliJ IDEA (powered by FernFlower decompiler)
package kickass._65xx;

public enum _65xxArgType {
    noArgument(0, 0, 0, "NONE"),
    immediate(1, 1, 1, "IMMEDIATE"),
    zeropage(2, 1, 1, "ZEROPAGE"),
    zeropageX(3, 1, 1, "ZEROPAGEX"),
    zeropageY(4, 1, 1, "ZEROPAGEY"),
    indirectZeropageX(5, 1, 1, "IZEROPAGEX"),
    indirectZeropageY(6, 1, 1, "IZEROPAGEY"),
    absolute(7, 2, 1, "ABSOLUTE"),
    absoluteX(8, 2, 1, "ABSOLUTEX"),
    absoluteY(9, 2, 1, "ABSOLUTEY"),
    indirect(10, 2, 1, "INDIRECT"),
    relative(11, 1, 1, "RELATIVE"),
    indirectZeropage(12, 1, 1, "IZEROPAGE"),
    zeropageRelative(13, 2, 2, "ZPRELATIVE"),
    indirectX(14, 2, 1, "INDIRECTX"),
    // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    // START: ADDED FOR 65CE02/45GS02/HUC6280 SUPPORT
    indirectZeropageZ(15, 1, 1, "IZEROPAGEZ"),
    indirectStackZeropageY(16, 1, 1, "ISTACKZEROPAGEY"),
    immediateWord(17, 2, 1, "IMMEDIATEW"),
    relativeWord(18, 2, 1, "RELATIVEW"),
    indirect32ZeropageZ(19, 1, 1, "32IZEROPAGEZ"),
    indirect32Zeropage(20, 1, 1, "32IZEROPAGE"),
    immediateAndZeropage(21, 2, 2, "IMMEDIATEANDZP"),
    immediateAndAbsolute(22, 3, 2, "IMMEDIATEANDABSOLUTE"),
    immediateAndZeropageX(23, 2, 2, "IMMEDIATEANDZPX"),
    immediateAndAbsoluteX(24, 3, 2, "IMMEDIATEANDABSOLUTEX"),
    absoluteAbsoluteAbsolute(25, 6, 3, "ABSOLUTE3"),
    // END: ADDED FOR 65CE02/45GS02/HUC6280 SUPPORT
    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    unresolvedAbsolute(-1, 1, zeropage, relative, absolute, "*ABSOLUTE"),
    unresolvedAbsoluteX(-2, 1, zeropageX, null, absoluteX, "*ABSOLUTEX"),
    unresolvedAbsoluteY(-3, 1, zeropageY, null, absoluteY, "*ABSOLUTEY"),
    unresolvedIndirect(-4, 1, indirectZeropage, null, indirect, "*INDIRECT"),
    unresolvedIndirectX(-5, 1, indirectZeropageX, null, indirectX, "*INDIRECTX"),
    // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    // START: ADDED FOR 65CE02/HUC6280 SUPPORT
    unresolvedImmediate(-6, 1, immediate, null, immediateWord, "*IMMEDIATE"),
    unresolvedImmediateAndAbsolute(-7, 2, immediateAndZeropage, null, immediateAndAbsolute, "*IMMEDIATEANDABSOLUTE"),
    unresolvedImmediateAndAbsoluteX(-8, 2, immediateAndZeropageX, null, immediateAndAbsoluteX, "*IMMEDIATEANDABSOLUTEX");
    // END: ADDED FOR 65CE02/HUC6280 SUPPORT
    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


    private final String name;
    private final int idNo;
    private int byteSize;
    private final int noOfArgs;
    private _65xxArgType zpForm = null;
    private _65xxArgType relForm = null;
    private _65xxArgType absForm = null;

    _65xxArgType(int idNo, int byteSize, int noOfArgs, String name) {
        this.idNo = idNo;
        this.byteSize = byteSize;
        this.noOfArgs = noOfArgs;
        this.name = name;
    }

    public String toString() {
        return this.name;
    }

    _65xxArgType(int idNo, int noOfArgs, _65xxArgType zpForm, _65xxArgType relForm, _65xxArgType absForm, String name) {
        this.idNo = idNo;
        this.noOfArgs = noOfArgs;
        this.zpForm = zpForm;
        this.relForm = relForm;
        this.absForm = absForm;
        this.name = name;
    }

    public int getIdNo() {
        return this.idNo;
    }

    public int getByteSize() {
        return this.byteSize;
    }

    public int getNoOfArg() {
        return this.noOfArgs;
    }

    public boolean isResolved() {
        return this.idNo >= 0;
    }

    public _65xxArgType getZpForm() {
        return this.zpForm;
    }

    public _65xxArgType getAbsForm() {
        return this.absForm;
    }

    public _65xxArgType getRelForm() {
        return this.relForm;
    }
}
