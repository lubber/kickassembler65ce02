#!/bin/bash
# Test the modified KickAssembler

# SWITCH TO JAVA VERSION 8
export JAVA_HOME=`/usr/libexec/java_home -v 1.8`
$JAVA_HOME/bin/java -version

function compile_asm() {
  echo Compiling $1
  $JAVA_HOME/bin/java -jar out/KickAss65CE02.jar test/examples/$1.asm -odir ../../out/examples -bytedumpfile $1.txt > out/out.txt  || { cat out/out.txt ; exit 1; }
}

function compare_asm() {
  echo Comparing $1
  diff out/examples/$1.txt test/ref/$1.txt || exit 1
}

for i in test/examples/*; do
  i=${i%.*}; i=${i##*/};
  compile_asm $i;
  compare_asm $i;
done;
