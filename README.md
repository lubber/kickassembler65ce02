# KICKASSEMBLER 65CE02/HuC6280/45GS02

This is a modified version of KickAssembler that also supports the 65CE02, HuC6280 and the 45GS02 CPUs.

### KickAssembler

KickAssembler is a brilliant 6502 family cross-assembler written in Java by Mads Nielsen. You find it here http://theweb.dk/KickAssembler.

### Download

You can download the newest version of the modified KickAssembler with 65CE02, HuC6280 and the 45GS02 support here

Releases: https://gitlab.com/jespergravgaard/kickassembler65ce02/-/releases

### The new CPUs

The **65CE02** is a 6502-family CPU that was most notably used in the Commodore 65. You can read about it here https://en.wikipedia.org/wiki/CSG_65CE02. 
It extends the 65C02 with several features, such as an extra register and new addressing modes. 
- New index register `Z` with instructions
- Indirect Zeropage Indexed by Z `lda ($12),z` 
- Stack Pointer Indirect Indexed `sta ($12,sp),y`
- Immediate Word `phw #$1234`
- Long Branches `lbra #$1234` 

The **HuC6280** is a 6502-family CPU that was created by Hudson Soft for use in NEC's TurboGrafx-16 PC Engine console.
You can read about it here https://en.wikipedia.org/wiki/Hudson_Soft_HuC6280.
It extends the 65C02 with a memory management unit and a bunch of new instructions and new addressing modes.
- Immediate and Zeropage `tst #imm,zp`
- Immediate and Absolute `tst #imm,abs`
- Immediate and Zeropage X indexed `tst #imm,zp,x`
- Immediate and Absolute X indexed `tst #imm,abs,x`
- Absolute, Absolute, Absolute `tia abs,abs,abs`

The **45GS02** is the newest 6502-family CPU, and will be the CPU of the new Mega65 which will be released soon. You can read about it here https://mega65.org/.
It extends the 65CE02 with features for handling 32-bit data and 32-bit memory access.
- New virtual 32-bit register `Q` with instructions   
- 32-bit Indirect Zeropage Z `lda (($12)),z` 
- 32-bit Indirect Zeropage `stq (($12))` 

An overview of the instruction set of 65CE02, HUC6280 and 45GS02 can be seen here https://airtable.com/shrRRDDvm3dvzV2Y1

## Using with Visual Studio Code 

The Visual Studio Code extension "Kick Assembler (C64) for Visual Studio Code" by Paul Hocker supports Kick Assembler 65CE02.

1. Install Visual Studio Code https://code.visualstudio.com/
2. Install the Visual Studio Code extension (ID: paulhocker.kick-assembler-vscode-ext)
3. Go to "Preferences" > "Settings" > "Extensions" > "Kick Assembler"
4. Set the setting "Kickassembler: Assembler Jar" to the full path to the Kick Assembler 65CE02 JAR file.
5. Set the setting "Kickassembler: Assembler Main Class" to "kickass.KickAssembler65CE02".
6. Configure the rest of the settings (Java Runtime Path, Emulator Path etc.) as described in Pauls documentation.

## Updated KickAssembler Documentation

#### Argument Types
Original: http://theweb.dk/KickAssembler/webhelp/content/ch03s02.html

Kick Assembler 65CE02/Huc6280/45GS02 uses the traditional notation for addressing modes / argument types:

Table 3.2. Argument Types

| Mode	                                         | Example                 |
|------------------------------------------------|-------------------------|
| No argument	                                 | `nop`                   |
| Immediate	                                     | `lda #$30`              |
| Zeropage	                                     | `lda $30`               |
| Zeropage,x	                                 | `lda $30,x`             |
| Zeropage,y	                                 | `ldx $30,y`             |
| Indirect zeropage,x	                         | `lda ($30,x)`           |
| Indirect zeropage,y	                         | `lda ($30),y`           |
| Abolute	                                     | `lda $1000`             |
| Absolute,x	                                 | `lda $1000,x`           |
| Absolute,y	                                 | `lda $1000,y`           |
| Indirect	                                     | `jmp ($1000)`           |
| Relative to program counter	                 | `bne loop`              |
| Indirect zeropage <br>(65C02+)                 | `adc ($12)`             |
| Zeropage, Relative <br>(65C02+)                | `bbr1 $12,label`        |
| Indirect,x <br>(65C02+)	                     | `jmp ($1234,x)`         |
| Indirect Zeropage Indexed by Z <br>(65CE02+)   | `lda ($12),z`           |
| Stack Pointer Indirect Indexed <br>(65CE02+)   | `sta ($12,sp),y`        |
| Immediate Word <br>(65CE02+)                   | `phw #$1234`            |
| Word Relative to program counter <br>(65CE02+) | `lbra loop`             | 
| Immediate, Zeropage <br>(HuC6280)              | `tst #12,34`            |
| Immediate, Absolute <br>(HuC6280)              | `tst #12,$3456`         |
| Immediate, Zeropage,x <br>(HuC6280)            | `tst #12,34,x`          |
| Immediate, Absolute,x <br>(HuC6280)            | `tst #12,$3456,x`       |
| Absolute, Absolute, Absolute <br>(HuC6280)     | `tia $1234,$5678,$9abc` |
| 32-bit Indirect Zeropage Z <br>(45GS02)        | `lda (($12)),z`         | 
| 32-bit Indirect Zeropage <br>(45GS02)          | `stq (($12))`           | 

### CPUs

(Original: http://theweb.dk/KickAssembler/webhelp/content/cpt_BasicAssemblerFunctionality.html)

Kick Assembler supports different sets of opcodes. The default set includes the standard 6502 mnemonics plus the illegal opcodes. To switch between instruction sets of different cpu's use the .cpu directive: The following will switch to the 65c02 instruction set:

```asm
.cpu _65c02   
loop:  inc $20
       bra loop   // bra is not present in standard 6502 mnemonics 
```

Available cpus are:

Table 3.1. CPU's

| Name	            |  Description | 
|-------------------|--------------|
| _6502NoIllegals	|  The standard 6502 instruction set. | 
| _6502	            |  The standard 6502 instruction + the illegal opcodes. <br>This is the default setting for KickAssembler. | 
| dtv	            |  The standard 6502 instruction set + the DTV commands. | 
| _65c02	        |  The 65c02 instruction set. | 
| _65ce02	        |  The 65ce02 instruction set. | 
| _huc6280	        | The HuC6280 instruction set. | 
| _45gs02	        |  The 45gs02 instruction set. | 


## Building KickAssembler 65CE02 edition

##### Prerequisites 
- A Java Compiler (I use OpenJDK 14.0.1)
- A Mac (I use MacOs 10.15 Catalina). This will probably also work on Linux or Windows with MSYS/MinGW/Cygwin since the shell-script is pretty simple, but I have not tested it.   

##### Step by step
  1. Clone this repository `git clone https://gitlab.com/jespergravgaard/kickassembler65ce02.git`
  1. Run `make.sh`
  2. If successful the modified JAR can be found in the `/out/` directory
  
#### `The make.sh` Script

The script works by downloading the newest version of KickAssembler and patching it with a few additional bits of Java-code to add support for the
new CPUs with their instruction sets and the new addressing modes.

The script has been confirmed to work with KickAssembler version 5.24.   

NOTICE: If Mads releases a version of KickAssembler supporting the additional CPUs this will be deleted. If KickAssembler internals change too much this will stop working. 
