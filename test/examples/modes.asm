// Test addressing modes

.cpu _6502

// none
rol
ror
txa
nop

// #imm
lda #0
ora #$1
ldx #(7+2)

// absolute
lda $1
sta $5555
ora $aaaa
cmp [9*3+2]

// absolute,x
lda 7,x
ora 1234,x
cmp 123+765,x

// absolute,y
lda 7,y
ora 1234,y
cmp 123+765,y

// (indirect,x)
lda (7,x)
ora (7+7,x)

// (indirect),y
lda (7),y
ora (7+7),y

// indirect absolute
jmp (7)
jmp (7000)
jmp (5*6+7/2)

.cpu _65c02
lbl:

// zp,rel
bbr0 7,lbl
bbr0 7+9,lbl+9

// (indirect zp)
ora (7)
ora (7*2+9)

// (absolute,x)
jmp (1,x)
jmp (1000,x)
jmp (1000/7+9,x)

.cpu _65ce02

// (indirect),z
lda (7),z
lda (7+7*3),z

// (offset,sp),y
lda (7,sp),y
lda (9+3*2,sp),y

.cpu _45gs02

// (indirect),z
ldq (7),z

// ((indirect32))
stq ((7))
stq ((7+9*3))

// ((indirect32)),z
lda ((7)),z
lda ((7+9*3)),z
ldq ((7+9*3)),z

.cpu _huc6280

// #imm,zp
tst #7,7
tst #7+3*2,7+3*2

// #imm,abs
tst #7,1000
tst #7+3*2,700+3*2

// #imm,zp,x
tst #7,7,x
tst #7+3*2,7+3*2,x

// #imm,abs,x
tst #7,1000,x
tst #7+3*2,700+3*2,x
