// A super simple C64 program

// Set the CPU
.cpu _6502

BasicUpstart2(start)

* = $0810
start:
    ldx #0
!:
    lda hello,x
    beq end
    sta $0400,x
    inx
    bne !-
end:
    rts




hello: .text @"hello world\$00"



