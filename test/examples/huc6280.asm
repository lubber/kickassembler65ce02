// Sample program for HUC6280
// Tests all new instructions

.cpu _huc6280

.pc = $2000
    sxy
    st0 #$55
    st1 #$aa
    sax
    st2 #$be
    say
    tma #2
    bsr !+
    tam #4
    csl
    cla
    clx
    cly
    csh
    ldx #2
    lda $3
    set
    adc $4
!:
    rts


