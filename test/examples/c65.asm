// Creates a simple program for the C65 that puts '*@*' on the screen
// Tests KickAssembler support for CPU 65CE02 CPU instructions and addressing modes

// Set the CPU
.cpu _65ce02

// Basic Upstart for C65
.pc = $2001 "Basic"
.byte $0a, $20, $0a, $00, $fe, $02, $20, $30, $00           // 10 BANK 0
.byte $15, $20, $14, $00, $9e, $20, $38, $32, $31, $35, $00 // 20 SYS 8215
.byte $00, $00                                              //

.pc = $2017 "Program"

.label SCREEN = $800
.label screen_zp = $2

main: {
    lda #'*'
    sta SCREEN
    ldz #<SCREEN
    stz SCREEN+1
    stz screen_zp
    ldz #>SCREEN
    stz screen_zp+1
    ldz #2
    sta ($2),z
    rts
    // Instructions using C65CE02 addressing modes
    // Indirect Zeropage Z ($12),z
    sta ($2),z
    // Stack Pointer Indirect Indexed ($12,SP),Y
    sta ($12,sp),y
    // Zeropage Relative
    bbr0 $1, main
    // Immediate Word
    phw #$1234
    phw #$12
    // EOM
    eom
    // INW zeropage
    inw $12
    // ASR zeropage
    asr $12
    // LDZ Immediate byte (should be truncated)
    ldz #$1234
    // Short branch (backward)
    lbeq main
    // Short branch (forward)
    lbmi next
next:
    // Long branch forward
    lbsr far
}
.pc = $3000 "Far away"
far:
    // Long branch backward
    lbra main
