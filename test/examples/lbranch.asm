// Creates a backward long branch

// Set the CPU
.cpu _65ce02

// Basic Upstart for M65
.pc = $2001 "Basic"
.byte $0a, $20, $0a, $00, $fe, $02, $20, $30, $00           // 10 BANK 0
.byte $15, $20, $14, $00, $9e, $20, $38, $32, $31, $35, $00 // 20 SYS 8215
.byte $00, $00                                              //

.pc = $2017 "Program"

main: {
    nop
    lda #0
    lbeq main
    lbeq main2
main2:
    rts
}
