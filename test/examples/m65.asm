// Creates a simple program for the M65
// Tests KickAssembler support for CPU 45GS02 CPU instructions and addressing modes

// Set the CPU
.cpu _45gs02

// Basic Upstart for M65
.pc = $2001 "Basic"
.byte $0a, $20, $0a, $00, $fe, $02, $20, $30, $00           // 10 BANK 0
.byte $15, $20, $14, $00, $9e, $20, $38, $32, $31, $35, $00 // 20 SYS 8215
.byte $00, $00                                              //

.pc = $2017 "Program"

.label COLORRAM = $FF80000
.label SCREEN = $800
.label colorram_zp = $2

main: {
    // Setup 32-bit zeropage
    ldz #<COLORRAM
    stz colorram_zp
    ldz #>COLORRAM
    stz colorram_zp+1
    ldz #<COLORRAM>>$10
    stz colorram_zp+2
    ldz #>COLORRAM>>$10
    stz colorram_zp+3
    ldz #2
    // 32-bit addressing
    lda (($2)),z
    adc (($2)),z
    sta (($2)),z
    ldq SCREEN
    sta SCREEN+40
    stx SCREEN+41
    sty SCREEN+42
    stz SCREEN+43
    //stq SCREEN+40
    rts
    // 32-bit virtual register
    ldq (colorram_zp),y
    stq (colorram_zp,sp),y
    bitq $12
    asrq $12,x
    cpq (colorram_zp)
    ldq ($12),z
    // 32-bit virtual register 32-bit addressing
    ldq (($12)),z
    adcq (($12))
    stq (($12))
    rts
}
