// Test CmdArgument()

.pseudocommand detect_type arg {
    .printnow arg.getType()
    .byte arg.getType()
}


* = $0800
    .printnow "AT_ABSOLUTE "  +AT_ABSOLUTE
    .printnow "AT_ZEROPAGE "  +AT_ZEROPAGE
    .printnow "AT_ABSOLUTEX " +AT_ABSOLUTEX
    .printnow "AT_ABSOLUTEY " +AT_ABSOLUTEY
    .printnow "AT_IMMEDIATE " +AT_IMMEDIATE
    .printnow "AT_INDIRECT "  +AT_INDIRECT
    .printnow "AT_IZEROPAGEX "+AT_IZEROPAGEX
    .printnow "AT_IZEROPAGEY "+AT_IZEROPAGEY
    .printnow "AT_NONE "      +AT_NONE

    detect_type 1234
    detect_type 12
    detect_type 12,x
    detect_type 12,y
    detect_type #$1234
    detect_type (12)
    detect_type (12,x)
    detect_type (12),y
    detect_type 

